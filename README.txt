
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration


INTRODUCTION
------------

User Request Name was born after an IRC chat about the idea to enable Drupal to
offer similar behaviour line the 'Request password' form for usernames.

User Request Name is just a small module that offers a way for users to retrieve
their username when they forget it. It will send the user an email containing
only their username after a user provides an email address.

This functionality is available on the user/username page. Just like the
'Create- account', 'Login' and 'Request password', an extra tab is rendered
called 'Request username'.

This module is inspired by: user registration password, username check, the core
user module, logintobbogan, email registration and others we forgot to mention.


INSTALLATION
------------

Installation is like any other module, just place the files in the
sites/all/modules directory and enable the module on the modules page.


CONFIGURATION
-------------

Visit admin/config/people/user_requestname to configure the module.

"Enable the 'Request username' link on the login block" offers a way to render
a link to the "Request username" form. By default an extra link will be
rendered in the login block, you can overwrite this via this option.

"Enable the 'Request username' e-mail verification" offers a way to suppress
confirmation of the e-mail address provided by a visitor before and during
submit of the form. If this option is disabled (No is selected), the submit
will report "Further instructions have been sent to your e-mail address." for
every action for every case. True, false, blocked, existing, or not, MX record
valid, or not.

You can configure the e-mail Drupal sends via the 'admin/config/people/accounts'
page. Look for 'Username recovery' at the bottom of the list.

For multilingual sites: i18n / variables are supported for the e-mail template.
Be sure to enable them at the admin/config/regional/i18n/variable page and to
translate them via the admin/config/regional/translate page.

Ones configured correctly, users will receive an e-mail in their default
language, setting available on user's edit page. It does not matter what the
site language is, this setting will be leading and overwrite the site's default
language. So it is logical and correct that if you have an German based site
with, let's say German and English languages enabled, and German is also the
site's default language, still when users have English as their default
language, they will receive an English e-mail reminding them about the username
they used to register to the site.

Install the flood_control module to configure flood settings.

<?php

/**
 * @file
 * user_requestname.admin.inc.
 */

/**
 * User requestname admin form.
 */
function user_requestname_settings() {
  $form = array();

  // Render an option to enable/disable the link on the login block.
  $form['user_requestname_render_login_block_link'] = array(
    '#type' => 'radios',
    '#title' => t('Render the %request_username link on the login block',
                  array('%request_username' => t('Request username'))),
    '#description' => t('Choose if a link should be rendered on the login
                         block that links to the request username form.'),
    '#options' => array(
      USER_REQUESTNAME_LOGINBLOCK_LINK_ENABLED => t('Yes'),
      USER_REQUESTNAME_LOGINBLOCK_LINK_DISABLED => t('No'),
    ),
    '#default_value' => variable_get('user_requestname_render_login_block_link',
      USER_REQUESTNAME_LOGINBLOCK_LINK_ENABLED),
  );
  // Render an option to enable/disable the privacy mode.
  $form['user_requestname_enforce_privacy'] = array(
    '#type' => 'radios',
    '#title' => t('Enforce privacy'),
    '#description' => t('Always return %message even if the account does
                         not exist.', array(
                           '%message' => t('Further instructions have been
                                            sent to your e-mail address.'))),
    '#options' => array(
      USER_REQUESTNAME_ENFORCE_PRIVACY_ENABLED => t('Yes'),
      USER_REQUESTNAME_ENFORCE_PRIVACY_DISABLED => t('No'),
    ),
    '#default_value' => variable_get('user_requestname_enforce_privacy',
      USER_REQUESTNAME_ENFORCE_PRIVACY_ENABLED),
  );

  return system_settings_form($form);
}
